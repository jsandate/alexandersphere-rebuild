import React from 'react';

function App() {
  return (
    <div>

      <Header />
      <RegionTop />
      <RegionFeatured />
      <RegionServices />
      <RegionTestimonials />
      <Footer />
    </div>
  );
}

export default App;
